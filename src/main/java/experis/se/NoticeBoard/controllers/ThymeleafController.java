package experis.se.NoticeBoard.controllers;

import experis.se.NoticeBoard.models.Notice;
import experis.se.NoticeBoard.models.UserEntity;
import experis.se.NoticeBoard.repositories.NoticeRepository;
import experis.se.NoticeBoard.repositories.ReplyRepository;
import experis.se.NoticeBoard.repositories.UserEntityRepository;
import experis.se.NoticeBoard.utils.SessionKeeper;
import org.apache.catalina.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpSession;
import java.util.ArrayList;
import java.util.Optional;

@Controller
public class ThymeleafController {

    @Autowired
    private NoticeRepository noticeRepository;


    @Autowired
    private UserEntityRepository userEntityRepository;


    @Autowired
    private ReplyRepository replyRepository;

    @GetMapping("/")
    public String getIndex(Model model, HttpSession session) {
        if(SessionKeeper.getInstance().checkSession(session.getId())) {
            Optional<UserEntity> optional = userEntityRepository.findById(SessionKeeper.getInstance().getUserEntityId(session.getId()));
            if(optional.isPresent()) {
                UserEntity userEntity = optional.get();
                model.addAttribute("loggedInUser", userEntity);
            } else {
                System.out.println("Couldn't find the user");
            }
        }
        // Call "findAll" in the repository to return all entries
        ArrayList<Notice> allNotices = (ArrayList<Notice>) noticeRepository.findAll();
        // Sort newest to oldest
        allNotices.sort((d1,d2) -> d2.date.compareTo(d1.date));

        model.addAttribute("notices", allNotices);
        return "index";
    }

    @GetMapping("/showNotice/{id}")
    public String getNoticeWithReplies(@PathVariable Integer id, Model model, HttpSession session) {
        if(SessionKeeper.getInstance().checkSession(session.getId())) {
            Optional<UserEntity> optional = userEntityRepository.findById(SessionKeeper.getInstance().getUserEntityId(session.getId()));
            if(optional.isPresent()) {
                UserEntity userEntity = optional.get();
                model.addAttribute("loggedInUser", userEntity);
            } else {
                System.out.println("Couldn't find the user");
            }
        }
        // Call "findAll" in the repository to return all entries
        Optional<Notice> optional = noticeRepository.findById(id);
        if(optional.isPresent()) {
            model.addAttribute("notice", optional.get());
            return "reply";
        } else {
            System.out.println("Couldn't find the id");
            return "redirect:/";
        }

    }

    @GetMapping("/new")
    public String getNewPost(Model model, HttpSession session) {
        if(!SessionKeeper.getInstance().checkSession(session.getId())) {
            return "redirect:/login";
        }
        Optional<UserEntity> optional = userEntityRepository.findById(SessionKeeper.getInstance().getUserEntityId(session.getId()));
        if(optional.isPresent()) {
            UserEntity userEntity = optional.get();
            model.addAttribute("loggedInUser", userEntity);
        } else {
            System.out.println("Couldn't find the user");
            return "redirect:/login";
        }
        return "new";
    }

    @GetMapping("/login")
    public String getLoginPage(Model model, HttpSession session) {
        if(SessionKeeper.getInstance().checkSession(session.getId())) {
            return "redirect:/";
        }
        return "login";
    }

    @GetMapping("/createAccount")
    public String getCreateNewAccountPage(Model model, HttpSession session) {
        if(SessionKeeper.getInstance().checkSession(session.getId())) {
            return "redirect:/";
        }
        return "createAccount";
    }

    @Autowired
    private RestControllers restControllers;

    @GetMapping("/logout")
    public String logoutAndRedirect(HttpSession session) {
        if(!SessionKeeper.getInstance().checkSession(session.getId())) {
            return "redirect:/";
        }
        restControllers.logoutSession(session);
        return "redirect:/";
    }
}
