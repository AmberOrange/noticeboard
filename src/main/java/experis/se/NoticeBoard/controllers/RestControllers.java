package experis.se.NoticeBoard.controllers;

import experis.se.NoticeBoard.models.Notice;
import experis.se.NoticeBoard.models.Reply;
import experis.se.NoticeBoard.models.UserEntity;
import experis.se.NoticeBoard.repositories.NoticeRepository;
import experis.se.NoticeBoard.repositories.ReplyRepository;
import experis.se.NoticeBoard.repositories.UserEntityRepository;
import experis.se.NoticeBoard.utils.CommonResponse;
import experis.se.NoticeBoard.utils.SessionKeeper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpSession;
import java.net.http.HttpResponse;
import java.util.Date;
import java.util.Optional;

@RestController
public class RestControllers {

    @Autowired
    private NoticeRepository noticeRepository;


    @Autowired
    private UserEntityRepository userEntityRepository;


    @Autowired
    private ReplyRepository replyRepository;

    // USER ENTITY RELATED --------------------------------------


    @PostMapping("/userEntity")
    public ResponseEntity<CommonResponse> createNewUserEntity(@RequestBody UserEntity userEntity) {
        CommonResponse cr = new CommonResponse();
        HttpStatus resp;
        if(!userEntityRepository.existsByUsername(userEntity.username)) {
            userEntity = userEntityRepository.save(userEntity);

            cr.data = userEntity;
            cr.message = "New user created!";

            resp = HttpStatus.CREATED;
        } else {
            cr.data = null;
            cr.message = "The user already exists";

            resp = HttpStatus.BAD_REQUEST;
        }

        return new ResponseEntity<>(cr, resp);
    }

    @RequestMapping(value = {"/userEntity/", "/userEntity/{id}"}, method = RequestMethod.GET)
    public ResponseEntity<CommonResponse> getUserEntity(@PathVariable(required = false) Integer id) {
        CommonResponse cr = new CommonResponse();
        HttpStatus resp;
        if (id == null) {
            cr.data = userEntityRepository.findAll();
            cr.message = "All users";

            resp = HttpStatus.OK;
        } else if(userEntityRepository.existsById(id)) {
            cr.data = userEntityRepository.findById(id);
            cr.message = "Found user with id " + id;

            resp = HttpStatus.OK;
        } else {
            cr.data = null;
            cr.message = "Couldn't find user with id " + id;

            resp = HttpStatus.NOT_FOUND;
        }
        return new ResponseEntity<>(cr, resp);
    }

    @DeleteMapping("/userEntity/{id}")
    public ResponseEntity<CommonResponse> deleteUserEntity(@PathVariable Integer id) {
        CommonResponse cr = new CommonResponse();
        HttpStatus resp;

        if(userEntityRepository.existsById(id)) {
            userEntityRepository.deleteById(id);
            cr.data = null;
            cr.message = "Delete user with id " + id;

            resp = HttpStatus.OK;
        } else {
            cr.data = null;
            cr.message = "Couldn't find user with id " + id;

            resp = HttpStatus.NOT_FOUND;
        }

        return new ResponseEntity<>(cr, resp);
    }


    // NOTICE RELATED --------------------------------------


    @PostMapping("/notice")
    public ResponseEntity<CommonResponse> createNewNotice(@RequestBody Notice notice, HttpSession session) {
        CommonResponse cr = new CommonResponse();
        HttpStatus resp;
        // Add information first
        if(SessionKeeper.getInstance().checkSession(session.getId()))
        {
            Integer loggedInId = SessionKeeper.getInstance().getUserEntityId(session.getId());
            Optional<UserEntity> optional = userEntityRepository.findById(loggedInId);
            notice.userEntity = optional.get();
            notice.date = new Date();
            notice = noticeRepository.save(notice);

            cr.data = notice;
            cr.message = "New post created!";

            resp = HttpStatus.CREATED;
        } else {
            cr.data = null;
            cr.message = "You're not logged in. Please login first.";

            resp = HttpStatus.FORBIDDEN;
        }

        return new ResponseEntity<>(cr, resp);
    }

    @RequestMapping(value = {"/notice/", "/notice/{id}"}, method = RequestMethod.GET)
    public ResponseEntity<CommonResponse> getNotice(@PathVariable(required = false) Integer id) {
        CommonResponse cr = new CommonResponse();
        HttpStatus resp;
        if (id == null) {
            cr.data = noticeRepository.findAll();
            cr.message = "All users";

            resp = HttpStatus.OK;
        } else if(noticeRepository.existsById(id)) {
            cr.data = noticeRepository.findById(id);
            cr.message = "Found user with id " + id;

            resp = HttpStatus.OK;
        } else {
            cr.data = null;
            cr.message = "Couldn't find user with id " + id;

            resp = HttpStatus.NOT_FOUND;
        }
        return new ResponseEntity<>(cr, resp);
    }

    @DeleteMapping("/notice/{id}")
    public ResponseEntity<CommonResponse> deleteNotice(@PathVariable Integer id) {
        CommonResponse cr = new CommonResponse();
        HttpStatus resp;

        if(noticeRepository.existsById(id)) {
            noticeRepository.deleteById(id);
            cr.data = null;
            cr.message = "Delete notice with id " + id;

            resp = HttpStatus.OK;
        } else {
            cr.data = null;
            cr.message = "Couldn't find user with id " + id;

            resp = HttpStatus.NOT_FOUND;
        }

        return new ResponseEntity<>(cr, resp);
    }

    // REPLY RELATED --------------------------------------

    @PostMapping("/reply")
    public ResponseEntity<CommonResponse> createNewReply(@RequestBody Reply reply) {
        reply.date = new Date();
        reply = replyRepository.save(reply);

        CommonResponse cr = new CommonResponse();
        cr.data = reply;
        cr.message = "New post created!";

        HttpStatus resp = HttpStatus.CREATED;

        return new ResponseEntity<>(cr, resp);
    }

    @RequestMapping(value = {"/reply/", "/reply/{id}"}, method = RequestMethod.GET)
    public ResponseEntity<CommonResponse> getReply(@PathVariable(required = false) Integer id) {
        CommonResponse cr = new CommonResponse();
        HttpStatus resp;
        if (id == null) {
            cr.data = replyRepository.findAll();
            cr.message = "All users";

            resp = HttpStatus.OK;
        } else if(replyRepository.existsById(id)) {
            cr.data = replyRepository.findById(id);
            cr.message = "Found user with id " + id;

            resp = HttpStatus.OK;
        } else {
            cr.data = null;
            cr.message = "Couldn't find user with id " + id;

            resp = HttpStatus.NOT_FOUND;
        }
        return new ResponseEntity<>(cr, resp);
    }

    @DeleteMapping("/reply/{id}")
    public ResponseEntity<CommonResponse> deleteReply(@PathVariable Integer id) {
        CommonResponse cr = new CommonResponse();
        HttpStatus resp;

        if(replyRepository.existsById(id)) {
            replyRepository.deleteById(id);
            cr.data = null;
            cr.message = "Delete user with id " + id;

            resp = HttpStatus.OK;
        } else {
            cr.data = null;
            cr.message = "Couldn't find user with id " + id;

            resp = HttpStatus.NOT_FOUND;
        }

        return new ResponseEntity<>(cr, resp);
    }

    // SESSION RELATED -------------

    @PostMapping("/sessionlogin")
    public ResponseEntity<CommonResponse> loginAndStartSession(@RequestBody UserEntity userEntity, HttpSession session) {
        CommonResponse cr = new CommonResponse();
        HttpStatus resp;
        if(userEntityRepository.existsByUsernameAndPassword(userEntity.username, userEntity.password)) {
            userEntity = userEntityRepository.getByUsernameAndPassword(userEntity.username, userEntity.password);
            System.out.println(userEntity.id + " " + session.getId());
            SessionKeeper.getInstance().addSession(session.getId(), userEntity.id);
            cr.data = userEntity;
            cr.message = "Logged in successfully!";
            resp = HttpStatus.OK;
        } else {
            cr.data = null;
            cr.message = "Couldn't find user";
            resp = HttpStatus.NOT_FOUND;
        }
        return new ResponseEntity<>(cr, resp);
    }

    @GetMapping("/sessionlogout")
    public ResponseEntity<CommonResponse> logoutSession(HttpSession session) {
        CommonResponse cr = new CommonResponse();
        HttpStatus resp;
        if(SessionKeeper.getInstance().checkSession(session.getId())) {
            SessionKeeper.getInstance().removeSession(session.getId());

            cr.message = "Logged out successfully!";
            resp = HttpStatus.OK;
        } else {
            cr.data = null;
            cr.message = "Couldn't find session id";
            resp = HttpStatus.NOT_FOUND;
        }
        return new ResponseEntity<>(cr, resp);
    }

}
