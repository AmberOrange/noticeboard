package experis.se.NoticeBoard.utils;

import org.thymeleaf.util.DateUtils;

import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;

public class SessionKeeper {
    private static SessionKeeper instance;

    private class SessionInfo {
        public Integer userEntityId;
        public Date dateExpires;

        public SessionInfo(Integer userEntityId) {
            this.userEntityId = userEntityId;
            Calendar now = Calendar.getInstance();
            now.add(Calendar.MINUTE, 10);
            this.dateExpires = now.getTime();
        }
    }
    private HashMap<String, SessionInfo> savedSessions;

    private SessionKeeper() {
        savedSessions = new HashMap<>();
    }
    public static SessionKeeper getInstance() {
        if(instance == null) {
            instance = new SessionKeeper();
        }

        return instance;
    }

    public void addSession(String sessionId, Integer userEntityId) {
        savedSessions.put(sessionId, new SessionInfo(userEntityId));
    }

    public boolean checkSession(String sessionId) {
        if(savedSessions.containsKey(sessionId))
        {
            if(savedSessions.get(sessionId).dateExpires.after(new Date())) {
                return true;
            }
            savedSessions.remove(sessionId);
        }
        return false;
    }

    public Integer getUserEntityId(String sessionId) {
        if(checkSession(sessionId)) {
            return savedSessions.get(sessionId).userEntityId;
        } else {
            return -1;
        }
    }

    public boolean removeSession(String sessionId) {
        if(checkSession(sessionId)) {
            savedSessions.remove(sessionId);
            return true;
        } else {
            return false;
        }
    }
}
