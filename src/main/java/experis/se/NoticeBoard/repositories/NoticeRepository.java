package experis.se.NoticeBoard.repositories;

import experis.se.NoticeBoard.models.Notice;
import org.springframework.data.jpa.repository.JpaRepository;

public interface NoticeRepository extends JpaRepository<Notice, Integer> {
}
