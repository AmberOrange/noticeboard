package experis.se.NoticeBoard.repositories;

import experis.se.NoticeBoard.models.Reply;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ReplyRepository extends JpaRepository<Reply, Integer> {
}
