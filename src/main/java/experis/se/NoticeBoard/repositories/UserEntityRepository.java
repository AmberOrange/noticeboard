package experis.se.NoticeBoard.repositories;

import experis.se.NoticeBoard.models.UserEntity;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface UserEntityRepository extends JpaRepository<UserEntity, Integer> {
    List<UserEntity> getAllByUsernameAndPassword(String Username, String Password);
    UserEntity getByUsernameAndPassword(String Username, String Password);
    Boolean existsByUsernameAndPassword(String Username, String Password);
    Boolean existsByUsername(String Username);
}
