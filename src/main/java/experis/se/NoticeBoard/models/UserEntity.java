package experis.se.NoticeBoard.models;

import com.fasterxml.jackson.annotation.*;

import javax.persistence.*;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

@Entity
@JsonIdentityInfo(
        generator = ObjectIdGenerators.PropertyGenerator.class,
        property = "id")
public class UserEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    public Integer id;

    @Column(nullable = false)
    public String username;

    @JsonProperty( value = "password", access = JsonProperty.Access.WRITE_ONLY)
    @Column(nullable = false)
    public String password;

    @JsonGetter("notices")
    public List<String> notices() {
        if(notices == null) {
            return Collections.emptyList();
        } else {
            return notices.stream()
                    .map(notice -> {
                        return "/notice/" + notice.id;
                    }).collect(Collectors.toList());
        }
    }


    @OneToMany(targetEntity=Notice.class,cascade = CascadeType.ALL,
            fetch = FetchType.LAZY, orphanRemoval = true, mappedBy = "userEntity")
    public List<Notice> notices;
}
