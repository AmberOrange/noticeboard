package experis.se.NoticeBoard.models;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

import javax.persistence.*;
import java.util.Date;

@Entity
@JsonIdentityInfo(
        generator = ObjectIdGenerators.PropertyGenerator.class,
        property = "id")
public class Reply {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    public Integer id;

    @Column(nullable = false)
    public String text;

    @Column
    public Date date;

    @ManyToOne
    @JoinColumn(name = "notice_id")
    public Notice notice;

    @Column(nullable = false)
    public Integer userEntityId;
}
